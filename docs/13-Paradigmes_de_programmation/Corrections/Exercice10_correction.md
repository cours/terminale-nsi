??? note "Correction"
    1. `vol(N,rouen,_,_,_,_).`
    2. `vol(N,_,paris,_,_,_).`
    3. `vol(N,rouen, _,H,_, _), H < =1200.`
    4. `vol(N,_,paris,_,H,_), H >= 1400.`
    5. `vol(N,_,paris,_,H,P), H =< 1700, P >= 100.`
    6. `vol(N1,V1,_,H,_,_), vol(N2,V2,_,H,_,_), V1 \== V2.`
    7) `vol(N,_,_,D,A,_), A-D > 0200.`