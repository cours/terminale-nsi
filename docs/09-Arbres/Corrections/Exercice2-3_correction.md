??? note "Correction"
    Avec 3 noeuds :

    ```mermaid
    flowchart LR
    classDef Nil stroke:none,fill:none,color:none;
        subgraph a [Hauteur 2]
            direction TB
            A(( )) --- B(( ))
            A --- C(( ))

        end

        subgraph b [Hauteur 3]
            direction TB
            D(( )) --- E(( ))
            D ~~~ F(( )):::Nil
            E --- G(( ))
            E ~~~ H(( )):::Nil


        end

        subgraph c [Hauteur 3]
            direction TB
            I(( )) ~~~ J(( )):::Nil
            I --- K(( ))
            K ~~~ M(( )):::Nil
            K --- L(( ))

        end

        subgraph d [Hauteur 3]
            direction TB
            N(( )) --- O(( ))
            N ~~~ P(( )):::Nil
            O ~~~ Q(( )):::Nil
            O --- R(( ))

        end

        subgraph e [Hauteur 3]
            direction TB
            S(( )) ~~~ T(( )):::Nil
            S --- U(( ))
            U --- V(( ))
            U ~~~ W(( )):::Nil

        end

        a x--x | | b x--x | | c x--x | | d x--x | | e

    ```

    Avec 4 noeuds :

    ```mermaid
    flowchart LR
    classDef Nil stroke:none,fill:none,color:none;
        subgraph a [Hauteur 4]
            direction TB
            A(( )) --- B(( ))
            A ~~~ C(( )):::Nil
            B --- D(( ))
            B ~~~ E(( )):::Nil
            D --- F(( ))
            D ~~~ G(( )):::Nil


        end

        subgraph b [Hauteur 4]
            direction TB
            A1(( )) ~~~ B1(( )):::Nil
            A1 --- C1(( ))
            C1 ~~~ D1(( )):::Nil
            C1 --- E1(( ))
            E1 ~~~ F1(( )):::Nil
            E1 --- G1(( ))


        end

        subgraph c [Hauteur 4]
            direction TB
            A2(( )) --- B2(( ))
            A2 ~~~ C2(( )):::Nil
            B2 ~~~ D2(( )):::Nil
            B2 --- E2(( ))
            E2 --- F2(( ))
            E2 ~~~ G2(( )):::Nil


        end

        subgraph d [Hauteur 4]
            direction TB
            A3(( )) ~~~ B3(( )):::Nil
            A3 --- C3(( ))
            C3 --- D3(( ))
            C3 ~~~ E3(( )):::Nil
            D3 ~~~ F3(( )):::Nil
            D3 --- G3(( ))


        end

        subgraph e [Hauteur 4]
            direction TB
            A4(( )) ~~~ B4(( )):::Nil
            A4 --- C4(( ))
            C4 --- D4(( ))
            C4 ~~~ E4(( )):::Nil
            D4 --- F4(( ))
            D4 ~~~ G4(( )):::Nil

        end

        subgraph f [Hauteur 4]
            direction TB
            A5(( )) --- B5(( ))
            A5 ~~~ C5(( )):::Nil
            B5 ~~~ D5(( )):::Nil
            B5 --- E5(( ))
            E5 ~~~ F5(( )):::Nil
            E5 --- G5(( ))

        end

        a x--x | | b x--x | | c x--x | | d x--x | | e x--x | | f

    ```

    ```mermaid
    flowchart LR
    classDef Nil stroke:none,fill:none,color:none;
        subgraph a [Hauteur 3]
            direction TB
            A(( )) --- B(( ))
            A ~~~ C(( )):::Nil
            B --- D(( ))
            B --- E(( ))


        end

        subgraph b [Hauteur 3]
            direction TB
            A1(( )) ~~~ B1(( )):::Nil
            A1 --- C1(( ))
            C1 --- D1(( ))
            C1 --- E1(( ))


        end

        subgraph c [Hauteur 3]
            direction TB
            A4(( )) --- B4(( ))
            A4 --- C4(( ))
            B4 ~~~ F4(( )):::Nil
            B4 --- G4(( ))


        end

        subgraph d [Hauteur 3]
            direction TB
            A3(( )) --- B3(( ))
            A3 --- C3(( ))
            C3 --- D3(( ))
            C3 ~~~ E3(( )):::Nil


        end

        subgraph e [Hauteur 3]
            direction TB
            A2(( )) --- B2(( ))
            A2 --- C2(( ))
            B2 --- D2(( ))
            B2 ~~~ E2(( )):::Nil


        end

        subgraph f [Hauteur 3]
            direction TB
            A5(( )) --- B5(( ))
            A5 --- C5(( ))
            C5 ~~~ D5(( )):::Nil
            C5 --- E5(( ))


        end

        a x--x | | b x--x | | c x--x | | d x--x | | e x--x | | f

    ```
