??? note "Correction"
    1. Codage
    <br>
    En suivant l’algorithme de Huffman, Dessiner les arbres des Huffman, et donner les tables de codages des mots suivants : 
        - Attention<br>
        ![](../Corrections/huffman1.png)
        - Institutionnalisation<br>
        ![](../Corrections/huffman2.png)
    2. Décodage
    <br>
    On vous donne la série binaire et l’arbre de Huffman suivants :<br>
    110101101101001001111000010110011001111010101111111100100111010111011101110101000010011001100000001<br>
    ![](../Corrections/huffman3.png)<br>
    Décoder la série binaire et donner le texte en clair. <br>
    'Warriors deux mille vingt'