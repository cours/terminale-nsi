# Sources

## Liens utiles

[https://fr.vittascience.com/learn/tutorial.php?id=40](https://fr.vittascience.com/learn/tutorial.php?id=40])

[https://nsi-snt.ac-normandie.fr/maqueen-avec-mu-editor](https://nsi-snt.ac-normandie.fr/maqueen-avec-mu-editor)

[https://lecluseo.scenari-community.org/CircuitPython/co/g_maqueen.html](https://lecluseo.scenari-community.org/CircuitPython/co/g_maqueen.html)


[https://introduction-to-robotics-using-maqueen.readthedocs.io/en/latest/tutorials/maqueen-programming-python.html](https://introduction-to-robotics-using-maqueen.readthedocs.io/en/latest/tutorials/maqueen-programming-python.html)

[https://www.pedagogie.ac-nantes.fr/enseignements-informatiques/enseignement/nsi/challenge-robot-maqueen-1447869.kjsp](https://www.pedagogie.ac-nantes.fr/enseignements-informatiques/enseignement/nsi/challenge-robot-maqueen-1447869.kjsp)

[https://dane.ac-bordeaux.fr/robotique/wp-content/uploads/sites/7/2021/06/Evolutivite-et-fichiers-du-robot.pdf](https://dane.ac-bordeaux.fr/robotique/wp-content/uploads/sites/7/2021/06/Evolutivite-et-fichiers-du-robot.pdf)