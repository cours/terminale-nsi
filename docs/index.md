# Bienvenue sur le site Terminale NSI

Vous trouverez ici :

- le cours de Terminale NSI
- les corrigés des exercices.

Pour répondre aux exercices de programmation, vous aurez par moment à votre disposition un IDE comme celui-ci :

![](images/IDE.JPG)

Vous pouvez y écrire votre programme, le modifier comme vous voulez.

Pour tester le résultat, vous pouvez cliquer sur le bouton ![](images/play.JPG), une console s'ouvrira alors sur la droite ou en dessous, et vous pourrez faire tous les tests voulus.

Pour valider le résultat, il faudra cliquer sur ![](images/valide.JPG)

Vous pouvez recharger le programme d'origine en cliquant sur ![](images/recharge.JPG)

Vous pouvez aussi (et cela est fortement conseillé) **enregistrer vos résultats** !