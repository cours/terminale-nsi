??? note "Correction"

    1. G a 6 sommets.
    2. G a 6 arcs.
    3. G est connexe.
    4. 1 possède 2 sommets adjacents (2 et 5).
    5. 4 possède 2 sommets adjacents (3 et 5).
    6. Il existe 2 chemins simples entre 2 et 4.
    7. Il existe 3 chemins simples entre 2 et 6.
    8. Il existe 2 chemins élémentaires entre 2 et 4.
    9. Il existe 1 chemin élémentaire entre 2 et 6.