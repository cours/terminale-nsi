??? note "Correction"

    1. G possède 7 sommets.
    2. G possède 10 arcs.
    3. G est connexe.
    4. G est fortement connexe.
    5. 1 possède 1 sommet adjacent (2).
    6. 4 possède 2 sommets adjacents (1 et 7).
    7. Il existe 2 chemins simples entre 2 et 4.
    8. Il existe 4 chemins simples entre 2 et 7.
    9. Il existe 2 chemins élémentaires entre 2 et 4.
    10. Il existe 2 chemins élémentaires entre 2 et 7.