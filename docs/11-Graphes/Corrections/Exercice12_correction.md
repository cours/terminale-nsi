??? note "Correction"
    1. Le graphe est orienté car la matrice n’est pas symétrique.
    2. 
        ```mermaid
        graph
                A((A)) --> B((B))
                A --- C((C))
                C --> C
                D --> C
                B --> D
        ```

