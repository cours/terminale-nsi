??? note "Correction"
    
    ```mermaid
    graph
            A((A)) --- B((B))
            A --- D((D))
            A --- C((C))
            B --- C
            B --- D
            D --- C
            B --- E
            D --- E
    ```