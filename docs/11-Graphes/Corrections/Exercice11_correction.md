??? note "Correction"
    Un graphe non orienté aura une matrice d’adjacence symétrique car l’arête (u,v) est équivalente à l’arête (v,u);