??? note "Correction"
    A → C, D

    B → A, F, G

    C → A

    D → G

    E → B, F

    F → E

    G → B, D
    

    dic = {A: [C, D], ...}
