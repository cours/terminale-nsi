??? note "Correction"
    
    ```mermaid
    graph
            A((A)) --- B((B))
            A --- E((E))
            B --- C((C))
            E --- F((F))
            E --- D((D))
            C --- F
            C --- D
            F --- D
    ```