# Préparer les épreuves du bac

## Epreuve écrite

Les sujets complets depuis quelques années, des exercices sont corrigés sur ce site :

[https://e-nsi.gitlab.io/ecrit/](https://e-nsi.gitlab.io/ecrit/){target='_blank'}

## Epreuve pratique

Des exercices d'entrainements (mais pas que!) sur ce site:

[https://e-nsi.gitlab.io/pratique/N1/](https://e-nsi.gitlab.io/pratique/N1/){target='_blank'}