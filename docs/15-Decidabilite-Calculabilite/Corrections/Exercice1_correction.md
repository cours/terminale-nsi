??? note "Correction"
    Les programmes qui manipulent d'autres programmes en tant que données sont courants. On peut citer :
    
    - Les compilateurs qui prennent en entrée un texte et le transforment en une suite de 0 et 1 exécutable par le microprocesseur de l'ordinateur 
    - L'interpréteur python fait de même, c'est d'ailleurs lui que nous avons mis à contribution dans notre machine universelle 
    - un système d'exploitation peut être vu comme un programme qui fait "tourner" d'autres programmes 
    - pour télécharger un logiciel on utilise un gestionnaire de téléchargement qui est lui-même un logiciel. 
    - Le navigateur internet en est un autre exemple : il reçoit un lot de données d'internet via le protocole http et interprète certaines d'entre elles comme programme (javascript) et d'autres comme données (html) etc... 