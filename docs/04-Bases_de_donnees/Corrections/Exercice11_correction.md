??? note "Correction"
    1. <br>
    **Commande** : (<u>Numéro de commande</u>, # numéro client, date)<br>
    **Client** : (<u>Numéro de client</u>, nom, adresse, ville)<br>
    **Produit** : (<u>Référence</u>, libelle, prix)<br>
    **Détail** : (<u># Référence, # numero de commande</u>, quantite)
    2. <br>
    ![](../Corrections/Exercice11.png)
