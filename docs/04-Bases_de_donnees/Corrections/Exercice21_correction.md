??? note "Correction"
    1.
    ```sql
    DELETE FROM livres
    WHERE titre= "Fondation";
    ```
    2.
    ```sql
    DELETE FROM livres
    WHERE idAuteur = 9;
    ```
    3.
    ```sql
    DELETE FROM livres
    WHERE annPubli ≤ 1945;
    ```