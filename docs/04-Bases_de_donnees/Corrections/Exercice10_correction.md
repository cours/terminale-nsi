??? note "Correction"
    1. **Commande** : Numéro de commande<br>
    **Client** : Numéro de client<br>
    **Produit** : Référence<br>
    **Détail** : Référence, numero de commande
    2. Pour une même quantité, il peut y avoir différentes références et différents numéros de commande
    3. Pour avoir l’unicité du détail d’un produit, il faut connaître la référence ET le numéro de commande .
    4. Clé étrangère : numéro client, référence et numéro de commande
