??? note "Correction"
    1)
    Entité : Commande<br>
    Attributs : numéro de commande, date

    Entité : Client <br>
    Attributs : numéro de client, nom, adresse, ville

    Entité : Produits <br>
    Attributs : référence, libellé, prix, quantité

    2) Commandes

    |Numéro de commande|Date|
    |:--:|:--:|
    |14010|24/09/2014|

    Clients

    |Numéro de client|Nom|Adresse|Ville|
    |:--:|:--:|:--:|:--:|
    |BD2014|LECOIN|24 RUE SAINT-JEAN|CAEN|

    Produits

    |Référence|Libellé|Prix|Quantité|
    |:--:|:--:|:--:|:--:|
    |190464K|Calcium chlorure 1 mol/l|77|10|
    |31535.292|Sodium chlorure 1 mol/l|105|15|
    |30024.290|Acide chlorhydrique 1 mol/l (1 N)|41|3|
    |30917.320|Iode 0,05 mol/l (0,1 N)|117|8|

    3) Non<br>
    4) Référence, Libellé, et Pris<br>
    5) Détails : Référence et Quantités<br>
    6) Il faudra faire des liens entre les tables.<br>
    Il y aura un problème du fait de l’absence de l’attribut numero commande
    dans la table Details et de l’absence de l’attribut numero client dans la table Commande)

