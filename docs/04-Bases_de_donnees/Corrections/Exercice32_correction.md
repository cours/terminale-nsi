??? note "Correction"
    1.
    ```sql
    INSERT INTO Departement
    VALUES (1, "76", "Seine-Maritime");
    ```
    2.
    ```sql
    INSERT INTO Ville
    VALUES (1, "Rouen", 76000, 110169, 1);
    ```
    3.
    ```sql
    INSERT INTO Ville
    VALUES (2, "Dieppe", 76200, 29080, 1), (3, "Envermeu", 76630, 2097, 1);
    ```
    4.
    ```sql
    INSERT INTO Departement
    VALUES (2, "27", "Eure");
    INSERT INTO Ville 
    VALUES (5, "Igoville", 27460, 1746, 2);
    ```
    5.
    ```sql
    INSERT INTO Ville
    VALUES (4, "Le Neubourg", 27110, 4166, 2);
    ```
