??? note "Correction"
    |IdCommune|Commune|Code Postal|Département|Nombre d’habitants|
    |:--:|:--:|:--:|:--:|:--:|
    |1|Rouen|76000|Seine-Maritime|110169|
    |2|Dieppe|76200|Seine-Maritime|29080|
    |3|Envermeu|76630|Seine-Maritime|2097|
    |4|Le Neubourg|27110|Eure|4166|
    |5|Igoville|27460|Eure|1746|

    |IdDepartement|Département|Code d’immatriculation|
    |:--:|:--:|:--:|
    |1|Seine-Maritime|76|
    |2|Eure|27|
    |3|Calvados|14|
