??? note "Correction"
    1. Redondances :
        - nom auteur (par ex : Bradbury)
        - prenom auteur (par ex : George)
        - editeur (par ex : Pocket)
        - langue (par ex : anglais)
        - annee publication (par ex : 1968)
    Eléments uniques :
        - titre
        - reference
    2. On rajouter la ligne :
        Chroniques martiennes, Bradbury, Ray, Denoël, anglais, 1950, 236984
    3. La référence est un élément unique qui référencera totalement un ouvrage.
    4. Il n’y a pas qu’un seul auteur, alors qu’il n’y a qu’une colonne pour cela...
