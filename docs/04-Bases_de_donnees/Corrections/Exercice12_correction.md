??? note "Correction"
    1) 2) 3)<br>
    **Auteur**(<u>idAuteur</u>, nomAuteur, prenomAuteur,langue)
    **Livre**(<u>idLivre</u>,titre,#idAuteur,annee)
    **Ouvrage**(<u>#idLivre</u>,quantité)

    Domaine de valeur :
        - idAuteur, idLivre : nombre entier strictement positif
        - nomAuteur, prenomAuteur, langue, titre, annee : toute chaîne de caractères
        - quantité : nombre entier positif ou nul

    3) ...
