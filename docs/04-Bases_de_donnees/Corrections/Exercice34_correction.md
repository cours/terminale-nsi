??? note "Correction"
    1.
    ```sql
    SELECT count(*)
    FROM Departement;
    ```
    2.
    ```sql
    SELECT count(*)
    FROM Ville;
    ```
    3.
    ```sql
    SELECT count(*)
    FROM Ville 
    WHERE idDepartement = 2;
    ```
    4.
    ```sql
    SELECT count(*)
    FROM Ville 
    WHERE idDepartement = 1;
    ```
    5.
    ```sql
    SELECT count(*)
    FROM Ville 
    WHERE idDepartement = 3;
    ```