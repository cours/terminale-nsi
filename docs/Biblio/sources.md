# Bibliographie et Sources

## Mutualisation Professeurs de NSI de Normandie :

- Delamare José, Lemesle Manuel, Lycée Blaise Pascal de Rouen: Bases de données
- Alexis Lecomte, Bizouarn Yann, Lycée André Maurois d'Elbeuf: Structures Linéaires, Programmation Orientée Objet
- Beaujault Catherine, Jugand Gwendal, Lycée Augustin Fresnel de Bernay: Structures Linéaires, Programmation Orientée Objet
- Bailly Luc, Durand Emmanuel, Lycée Les Fontenelles de Louvier: Structures Linéaires, Programmation Orientée Objet
- Devedeux Dominique, Deleu Philippe, Rossignol Manuel, Lycée Guy de Maupassant de Fécamp: Bonnes pratiques logicielles, Bases de données
- Lemesle Manuel, Pelletier Isabelle, Trapes Olivier, Lycée Flaubert de Rouen : Bases de données, Récursivité
- Dommanget Jean-François, Naillat Fabien, Lycée Marcel Sembat de Sotteville les Rouen : Programmation Orientée Objet
- Bazid Azedinne, Varela Julien, Lycée Edouard Delamare Deboutteville de Forges les Eaux: Programmation Orientée Objet
- Poilvert Franck, Lycée Jules Siegfried du Havre: Programmation Orientée Objet, Composants intégrés d'un système sur puce
- Ait Ouakli Mohand, Maillard Laurent, Lycée Modeste Leroy d'Evreux
- Arcangioli Christophe, Laboulais David, Weislinger Mathias, Lycée Guillaume le Conquérant de Lillebonne: Gestion des processus
- Avenel Sylvain, Leroy Christophe, Veron Christophe, Lycée Pablo Neruda de Dieppe: Gestion des processus: Graphes
- Barbier Jean-Matthieu, Denis Cyril, Lucas Loïc, Lycée Georges Dumezil de Vernon
- Batte Philippe, Lycée Anguier d'Eu: Bases de données, Programmation Orientée Objet
- Bossaert Fabrice, Gicquiaud Olivier, Lycée Porte de Normandie de Verneuil d'Avre et d'Iton
- Chardine Marc-Aurélien, Tetart Sandrine, Lycée Pierre Corneille de Rouen
- Delacour Pascal, Le Jan Eric, Tonon Nicolas, Lycée Camille Saint-Saens de Rouen: Programmation Orientée Objet
- Denir Arnaud, Dubois Rémi, Lycée Val de Seine de Grand Quevilly: Bases de données
- Goujon Thierry, Leclef Guillaume, Lycée de la côte d'Albatre de Saint Valéry en Caux: Programmation Orientée Objet
- Piolain Yoann, Lycée Jacques Prévert de Pont-Audemer : Bases de données
- Mermet Bruno, Université du Havre : cours du DIU


## Sites internet

- [Pixees](https://pixees.fr/informatiquelycee/)
- [qkzk.xyz](https://qkzk.xyz/docs/)
- [Lecluse](https://www.lecluse.fr/nsi/)
- [Blaise Pascal](https://info.blaisepascal.fr)


Un grand merci à tous!

