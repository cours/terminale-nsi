## Un peu d'histoire

Le jeu Pong est un des premiers jeux vidéo d'arcade.

Vous pourrez en trouver un descriptif [ici](https://fr.wikipedia.org/wiki/Pong){:target="_blank" }.

## Objectifs

- Par groupe de 2, jouer au mythique jeu Pong à l'aide de deux cartes Micro:Bit.
- Revoir les points de programmations de l'année de Première NSI

## Cahier des charges

- Chaque carte Micro:Bit devra avoir une raquette de 2 pixels verticaux sur le bord gauche ou droite.
![raquetteG.JPG](raquetteG.JPG) ![raquetteD.JPG](raquetteD.JPG)
- Chaque raquette peut bouger verticalement à l'aide des boutons A et B.
- Une balle d'un pixel navigue entre les deux cartes et est dirigée au moment de l'arrivée sur la raquette suivant l'inclinaison de la carte (plus elle est inclinée, plus la balle repart de la raquette verticalement, si la carte n'est pas inclinée, elle repart donc horizontalement)
- A chaque balle perdue, les deux cartes indiquent le score en cours.
- En appuyant sur le logo, on relance une nouvelle balle à partir de la carte perdante.
- Une partie s'arrête après 10 balles gagnées par un joueur, et on peut rejouer en secouant une des deux cartes.
- On pourra bien sûr y mettre un peu de son et de musique !

## Quelques rappels

Vous pouvez vous reférez au cours sur les IHM et au projet Jeu Balle de l'année de Première NSI.

En l'absence de carte, il existe ce [simulateur](https://fr.vittascience.com/microbit){:target="_blank" }, à utiliser de préférence sur le navigateur Edge.

Mais pour résumé :

- Bibliothèques :
```python
from microbit import *
from time import sleep # Pour faire des pauses
import music
```
- Effacer tous les pixels d'un coup : `display.clear()`
- Afficher un pixel aux coordonnées $(x,y)$ : `display.set_pixel(x, y, 9)`
- Utiliser le bouton A (ou B) :
```python
if button_a.is_pressed():
    while button_a.is_pressed():
        attente = 1
    ...
```
- Utiliser le bouton logo :
```python
if pin_logo.is_touched():
    while pin_logo.is_touched():
        attente = 1
    ...
```
- Secouer la carte :
```python
if accelerometer.current_gesture() == 'shake':
    while accelerometer.current_gesture() == 'shake':
        attente = 1
    ...
```
- Utiliser l'inclinaison de la carte :
```python
lecture = accelerometer.get_x() # ou accelerometer.get_y()
if lecture > 20: # 20 indique l'inclinaison de la carte, peut aller de -1000 à 1000
    ...
```

## La nouveauté : faire discuter deux cartes Micro:Bit

Le programme ci-dessous nécessite deux cartes : un appui sur un bouton de la carte émettrice provoque un affichage sur la carte réceptrice. En cas d'appui sur le bouton A, on affiche `Image.YES` et en cas d'appui sur le bouton B, on affiche `Image.NO`.

Programme sur la carte émettrice :

```python
from microbit import *
import radio

radio.config(group=1) # le numero du groupe devra correspondre
                      # à votre numéro de groupe donné par le professeur

radio.on()

while True:
    if button_a.is_pressed():
        while button_a.is_pressed():
            attente = 1
        radio.send("A")
    elif button_b.is_pressed():
        while button_b.is_pressed():
            attente = 1
        radio.send("B")
```

Programme sur la carte réceptrice :

```python
from microbit import *
import radio

radio.config(group=1) # le numero du groupe devra correspondre
                      # à votre numéro de groupe donné par le professeur

radio.on()

while True:
    incoming = radio.receive()
    if incoming == "A":
        display.show(Image.YES)
        sleep(200)
    elif incoming == "B":
        display.show(Image.NO)
        sleep(200)
    display.clear()
```

Sur le simulateur, pensez à activer le mode multi : ![multi.JPG](multi.JPG)

## Transférer un programme sur une carte Micro:Bit

Trois solutions s'offre à nous :

- Avec le navigateur Edge, Chrome ou Opera (tout dépendra du pc sur lequel vous travaillez) :<br>
Depuis le site du simulateur, branchez votre carte Micro:Bit via le cordon USB, puis cliquez sur **Téléverser** (éventuellement descendre le menu Télécharger.hex pour le voir)
- Depuis le simulateur, cliquez sur **Télécharger.hex** de manière à télécharger le fichier d'installation.<br>
Puis, une fois la carte branchée sur l'ordinateur, faire un glisser/deposer du fichier vers la carte à partir de l'explorateur de fichiers.
- Avec le logiciel Mu :<br>
Après avoir branché votre carte Micro:Bit via le cordon USB, copiez/collez le code python, puis cliquez sur **Flasher**


## Notation

- Programme : 10 points (un attention particulière sera mise sur la documentation et la clarté du code)
- Présentation orale : 10 points :
    - Présentation de 5 minutes (sans support autre qu'une feuille A4)
    - Questions pendant 10 minutes