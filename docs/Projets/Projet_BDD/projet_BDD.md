---
title: "Projet : Base de données, Python et Web"
---
## Objectif

Le but de ce projet est de pouvoir visualiser et modifier l'ensemble des données d'une Base de données dans une interface Web.

## Cahier des charges

- Le logiciel utilisé pour cela devra être Edupython (ou autre logiciel Python hors ENT)
- Le projet se fera individuellement ou en groupe de 2 maximum.
- Vous ferez votre projet sur une des trois bases de données à votre disposition (elles sont rangées par ordre de "longueur" de projet):
    - base sur les villes, départements et région de France:[`Region_Departement_Ville.db`](Documents/Region_Departement_Ville.db)
    - base sur une bibliothèque: [`LivresAuteurs.db`](Documents/LivresAuteurs.db)
    - base sur le cinéma (films, séances, ...): [`Cinema.sqlite`](Documents/Cinema.sqlite)
- Un bonus sera apporté si vous incluez une mise en forme agréable à vos pages Web.

## Exemple d'interface Web :

Voici une présentation **basique** de ce qui est attendu, grâce à une base de données contenant des régions et des départements :

- Page d'accueil<br>
<br>
![](Images/index.png){width=70%}
- Page d'ajout d'une région<br>
<br>
![](Images/ajout_region.png){width=70%}
- Page d'affichage des différentes régions<br>
<br>
![](Images/affiche_region.png){width=70%}
- Page pour entrer une requête quelconque<br>
<br>
![](Images/requete.png){width=70%}

## Utilisation de Python pour créer des pages web

### Création d'un serveur local

Pour pouvoir visualiser les pages web, nous allons créer un serveur local et l'exécuter sous Python. Il faudra laisser l'exécution en cours tout au long de la visualisation des pages.

Pour cela, téléchargez le fichier [Fichiers_exemples.zip](Documents/Fichiers_exemples.zip), dézippez-le puis ouvrez le fichier `serveur.py` avec Edupython et exécutez-le.<br>
Il n'est pas utile ici d'en comprendre le fonctionnement. Il faut juste repérer le port du serveur local (ici `8888`) qui peut être modifié éventuellement.

### Utilisation des fichiers Python pour créer du HTML


Dans un navigateur (Firefox de préférence), écrire dans la barre d'adresse : 

`localhost:8888/index.py` et valider.

Le fichier `index.py` crée le fichier `index.html`, n'hésitez pas à l'ouvrir pour voir comment il est construit !

Vous pouvez aussi ouvrir les autres fichiers Python mis à votre disposition.


On peut ainsi mêler le code Python avec du code HTML, et utiliser tout ce que vous connaissez déjà depuis la classe de Première.

## Interfaces d'utilisation de la base de donnée

Pour connaître l'ensemble des tables, il suffit d'écrire 

```sql
SELECT * 
FROM sqlite_master 
WHERE TYPE ="table";
``` 


{!{ sqlide titre="Base Villes..." base="Projets/Projet_BDD/Documents/Region_Departement_Ville.db" }!}


{!{ sqlide titre="Base Bibliothèque" base="Projets/Projet_BDD/Documents/LivresAuteurs.db" }!}


{!{ sqlide titre="Base Cinéma" base="Projets/Projet_BDD/Documents/Cinema.sqlite" }!}


## Rendu et notation

Vous devrez rendre individuellement et dans une archive (`.zip` ou `.7z`)  :

- un fichier présentant votre projet (Carnet de bord), à faire au fur et à mesure de l'avancé du projet.
- un fichier Python pour lancer un serveur Web.
- un fichier Python correspondant à la page d'accueil de l'interface Web.
- des fichiers Python correspondant aux autres page Web
- des fichiers Python permettant d'exécuter des requêtes sql.
- une diaporama, support d'une présentation oral.