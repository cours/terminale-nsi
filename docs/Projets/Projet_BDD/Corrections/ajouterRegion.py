#!/usr/bin/python

import style as st

print("Content-type: text/html; charset=iso-8859-1\n")
print('''
<html>
    <head>
        <title>Collectivites</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="style.css">
    </head>

    <body>''')

print('''<style type="text/css">''',st.css(),'''</style>''')

print('''
        <h1 style='text-align:center'>Ajouter une region</h1>

        <form method='get' action='insertionRegion.py'>
             Nom de la region : <input type='text' name='nomRegion'/>
            <input type='submit'value='Ajouter'/>
         </form>
        <hr/>
        <a href='index.py'>Retour au menu</a>
    </body>
</html>''')
