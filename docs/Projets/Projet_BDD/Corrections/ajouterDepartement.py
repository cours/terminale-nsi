#!/usr/bin/python

import sqlite3
import style as st
def select_region():
    retour = '<select name="idRegion">'
    connexion = sqlite3.connect('collectivites1.db')
    curseur = connexion.execute('''SELECT idRegion, nomRegion
                                   FROM Region
                                   ORDER BY nomRegion''')
    for region in curseur:
        option = "<option value='" + str(region[0]) + "'>"
        option += region[1]
        option += "</option>\n"
        retour += option
    retour += "</select>"
    connexion.close()
    return retour



print("Content-type: text/html; charset=iso-8859-1\n")
print('''
<html>
    <head>
        <title>Collectivites</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="style.css">
    </head>

    <body>''')

print('''<style type="text/css">''',st.css(),'''</style>''')

print('''
        <h1 style='text-align:center'>Ajouter un departement</h1>

        <form method='get' action='insertionDepartement.py'>
            Numero du departement : <input type='text' name='numeroDep'/><br/>
            Nom du departement : <input type='text' name='nomDep'/><br/>
            Region :''')

print(select_region())

print('''
            <br/>
            <input type='submit' value='Ajouter'/>
        </form>


        <hr/>
        <a href='index.py'>Retour au menu</a>

    </body>
</html>''')
