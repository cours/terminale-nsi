#!/usr/bin/python

import style as st

print("Content-type: text/html; charset=iso-8859-1\n")
print('''
<html>
    <head>
        <title>Collectivites</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="style.css">
    </head>

    <body>''')

print('''<style type="text/css">''',st.css(),'''</style>''')

print('''
        <h1 style='text-align:center'>Collectivites</h1>

        <ol>
            <li><a href='ajouterRegion.py'>Ajouter une region</a></li>
            <li><a href='afficherRegions.py'>Afficher la liste des regions</a></li>
            <li><a href='ajouterDepartement.py'>Ajouter un departement</a></li>
            <li><a href='afficherDepartements.py'>Afficher la liste des departements</a></li>
            <li><a href='requete.py'>Ecrire une requete</a></li>
        </ol>
    </body>
</html>''')
