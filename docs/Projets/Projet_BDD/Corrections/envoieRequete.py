﻿#!/usr/bin/python

import sqlite3
import cgi
import style as st

formulaire=cgi.FieldStorage()

nouvelle_requete=formulaire.getvalue("nomRequete")


connexion=sqlite3.connect('collectivites1.db')

curseur=connexion.execute(nouvelle_requete)

print("Content-type: text/html; charset=utf-8\n")
print('''
<html>
    <head>
        <title>Collectivites</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="style.css">
    </head>

    <body>''')

print('''<style type="text/css">''',st.css(),'''</style>''')


# récupération des noms des attributs si requete commence par select
if nouvelle_requete[0:6]=="SELECT":
    Liste_Attributs=[]
    noms_attributs_dans_requete=[]
    liste_mot_requete = nouvelle_requete.split(" ")
    place_FROM = liste_mot_requete.index('FROM')
    nom_table = liste_mot_requete[place_FROM+1]
    if liste_mot_requete[1] != '*':
        noms_attributs_dans_requete = liste_mot_requete[1].split(",")
##        for i in range(1,place_FROM):
##            noms_attributs_dans_requete.append(liste_mot_requete[i])
        Liste_Attributs = noms_attributs_dans_requete
    else:
        noms_attributs_dans_table = connexion.execute('pragma table_info('+nom_table+')')
        for nom in noms_attributs_dans_table :
            ligne=list(nom)
            Liste_Attributs.append(ligne[1])

    # Début du tableau
    print('''
            <h1 style='text-align:center'>Requete : '''+nouvelle_requete+'''</h1>

            <table border='1' <tr>''')
    for i in range(len(Liste_Attributs)):
        print("<th>"+Liste_Attributs[i]+"</th>")
    print("</tr>")

    # récupération des données
    for tuple in curseur:
        print("<tr>")
        ligne=list(tuple)
        for i in range(len(ligne)):
            print("<td>"+str(ligne[i])+"</td>")
        print("</tr>")

else:
    print('''
            <h1 style='text-align:center'>Requete : '''+nouvelle_requete+'''</h1>''')
    # récupération des données
    for tuple in curseur:
        print("<p>")
        ligne=list(tuple)
        for i in range(len(ligne)):
            print(" "+str(ligne[i])+" ")
        print("<p>")

connexion.commit()
connexion.close()

print('''</table>
        <hr/>
        <a href='index.py'>Retour au menu</a>
    </body>
</html>''')


