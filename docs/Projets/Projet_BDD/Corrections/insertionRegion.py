#!/usr/bin/python

import sqlite3
import cgi

formulaire = cgi.FieldStorage()

nouvelle_region = formulaire.getvalue("nomRegion")

connexion = sqlite3.connect('collectivites1.db')

#Pour générer une nouvel identifiant, on récupère l'identifiant max existant...
curseurId = connexion.execute('SELECT max(idRegion) FROM Region')
tupleId = curseurId.__next__()
idMax = int(tupleId[0])
#... et on rajoute 1
nouvel_id = idMax + 1
connexion.execute('INSERT INTO Region(idRegion, nomRegion) VALUES (?, ?)', (nouvel_id, nouvelle_region))
connexion.commit()
connexion.close()

import afficherRegions