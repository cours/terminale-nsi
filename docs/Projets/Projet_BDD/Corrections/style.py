#!/usr/bin/python


def css():
    msg='''
        body {
            background-color: silver;
        	margin-left:10%;
        }


        h1 {
            font-family: Comic Sans MS;
            font-style: normal;
            font-size: 2.5em;
            font-weight: bold;
            color: blue;
            text-align: center;
        }

        p {
            font-family: serif;
            font-style: normal;
            font-size: 1em;
            font-weight: normal;
            color: black;
            text-align: justify;
        }

        a { color : navy;

        }

        ol {list-style-type: lower-latin;

        }
    '''
    return msg