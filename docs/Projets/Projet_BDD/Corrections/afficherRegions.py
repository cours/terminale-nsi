#!/usr/bin/python
import sqlite3
import style as st

print("Content-type: text/html; charset=iso-8859-1\n")
print('''
<html>
    <head>
        <title>Collectivites</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="style.css">
    </head>

    <body>''')

print('''<style type="text/css">''',st.css(),'''</style>''')

print('''
        <h1 style='text-align:center'>Liste des regions</h1>

        <table border='1'
        <tr><th>Regions</th></tr>''')


connexion = sqlite3.connect('collectivites1.db')
curseur = connexion.execute("SELECT nomRegion FROM Region ORDER BY nomRegion")
for tuple in curseur:
    ligne = list(tuple)
    print("<tr><td>" + ligne[0] + "</td></tr>")
connexion.close()

print('''</table>

        <hr/>
        <a href='index.py'>Retour au menu</a>

    </body>
</html>''')
