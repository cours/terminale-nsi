class Lemming:
    ...

class Case:
    ...


class Jeu:
    
    ...

    def demarrer(self):
        while True:
            def animer():
                ... # lance un tour de jeu
                grille = ... # grille prête à être affichée
                titre.config(text = ...) # Affiche la grille avec les lemmings
                point.config(text = "Le score est de "+ str(...) + " lemming(s) sauvé(s)") # Affiche le score
                fenetre.after(1000, supprimer_texte) # lance la fonction supprimer_texte après 1s.

            def supprimer_texte():
                titre.config(text = "")
                fenetre.after(0, animer)

            def attente():
                fenetre.after(1000, supprimer_texte)

            def touche_clavier(event):
                t = event.keysym
                if t == ...: # si on appuie sur la touche l
                    ... # on ajoute un lemming sur la grille
                if t == ...: # si on appuie sur la touche pour quitter
                    fenetre.destroy()
                

            # Partie animation du jeu dans l'interface graphique
            fenetre = Tk() # Création de la fenêtre d'affichage
            grille = jeu.afficher() # grille prête à être affichée
            titre = Label(fenetre, text = ..., justify='left', font=("Courier", 30)) # Affiche la grille avec les lemmings 
            titre.pack()
            point = Label(fenetre, text = "Le score est de "+ str(...) + " lemming(s) sauvé(s)", font = ("Courier", 30))
            point.pack()
            attente()
            fenetre.bind("<Key>", touche_clavier) # Ecoute de l'appuie d'une touche sur la clavier
            fenetre.mainloop() # Actualisation de la fenêtre d'affichage (boucle infinie)


from time import sleep
from tkinter import *

#lem1 = Lemming(1, 1, 1)
#lem2 = Lemming(3, 4, -1)
#lem3 = Lemming(3, 9, 1)
#lem4 = Lemming(1, 5, 1)
#lem5 = Lemming(5, 8, 1)
#lem6 = Lemming(1, 3, 1)
#jeu = Jeu('lemmings.txt', [lem1, lem2, lem3, lem4, lem5, lem6])
jeu = Jeu('lemmings.txt', [])
jeu.demarrer()


