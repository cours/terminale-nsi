## Un peu d'histoire

[Lemmings](https://fr.wikipedia.org/wiki/Lemmings_(jeu_vid%C3%A9o,_1991)) est un jeu vidéo de réflexion développé par le studio écossais DMA Design (aujourd’hui Rockstar North) et édité par Psygnosis en 1991.

Le joueur doit guider des dizaines de lemmings, minuscules créatures écervelées, dans des niveaux alambiqués truffés de dangers mortels. Le jeu est fondé sur le mythe populaire selon lequel les lemmings, petits rongeurs bien réels des régions arctiques, se livreraient au suicide collectif en se jetant des falaises.



## Objectifs

- Par groupe de 2, programmer une animation du jeu Lemmings
- Approfondir la Programmation Orientée Objet
- Utiliser un interface graphique avec Tkinter de Python

## [Cahier des charges](https://info.blaisepascal.fr/lemmings){target="_blank"}

Dans ce jeu, les lemmings marchent dans une grotte représentée par une grille à deux dimensions dont chaque case est soit un mur soit un espace vide, un espace vide pouvant contenir au maximum un lemming à un instant donné. Les lemmings apparaissent les uns après les autres à une position de départ, et disparaissent lorsqu’ils atteignent une case de sortie.

Chaque lemming possède les propriétés suivantes :

- Sa position, donnée par deux coordonnées, désignant la case dans laquelle il se trouve.
- La direction dans laquelle il se déplace (gauche ou droite).

Les lemmings se déplacent à tour de rôle, toujours dans l’ordre correspondant à leur introduction dans le jeu, de la manière suivante :

- si l’espace immédiatement en-dessous est libre, le lemming tombe d’une case ;
- sinon, si l’espace immédiatement devant est libre (dans la direction du lemming concerné), le lemming avance d’une case ;
- enfin, si aucune de ces deux conditions n’est vérifiée, le lemming se retourne.

Aperçu du rendu final :

![](lemmings.gif)


## Structure du jeu

On propose, pour réaliser ce petit programme permettant de voir évoluer une colonie de lemmings, une structure avec une classe `Lemming` pour les lemmings, une classe `Case` pour les cases de la grotte, et une classe principale `Jeu` pour les données globales, définies par le diagramme suivant :

### Diagramme de classe :

![](diagramme.JPG)

### Classe `Lemming`

La classe `Lemming` a pour attributs des entiers positifs `l` et `c` indiquant la position (ligne et colonne) où se trouve le lemming dans la grille, et un attribut `d` indiquant sa direction, valant 1
si le lemming se dirige vers la droite et -1 si le lemming se dirige vers la gauche.

Cette classe fournit en outre les méthodes suivantes :

- `__str__ (self)` renvoie '>' ou '<' selon la direction du lemming ;
- `avancer(self)` déplace le lemming, dans la bonne direction, droite ou gauche (selon la direction dans laquelle il se trouve) ;
- `retourner(self)` retourne le lemming ;
- `tomber(self)` fait tomber le lemming ();
- ...

!!! attention "Attention"
    Le lemming ne regarde pas où il va.
    Ce n’est donc pas lui qui décide quoi faire s’il y a un mur en face, un autre lemming, un trou, ...
    Tout ceci est géré par la méthode `tour` de la classe `Jeu`.

### Classe `Case`

La classe `Case` contient un attribut `terrain` contenant le caractère représentant la caractéristique de la case (mur, vide, sortie, entrée, ..., comme dans le fichier texte décrivant la grotte), et un attribut `lemming` contenant l’éventuel lemming présent dans cette case et `None` si la case est libre.

Cette classe fournit notamment les méthodes suivantes :

- `__str__ (self)` renvoie le caractère à afficher pour représenter cette case ou son éventuel occupant ;
- `estLibre(self)` renvoie `True` si la case est peut recevoir un lemming (elle n’est ni un mur, ni occupée par un lemming) ;
- `liberer(self)` retire le lemming présent ;
- `occuper(self, lem)` place le lemming `lem`sur la case ;
- ...

### Classe `Jeu`

La classe principale `Jeu` contient un attribut `grille` contenant un tableau de cases (instances de Case) à deux dimensions , et un attribut `lemmings` contenant la liste des Lemmings actuellement en jeu.

Son constructeur initialise la grille, par exemple à partir d’une carte donnée par un fichier texte d’un format inspiré du suivant, où le caractère `#` représente un mur, `I` représente l’entrée et `O`
la sortie :

![](texte.JPG)

Cette classe fournit notamment (mais pas uniquement !) les méthodes suivantes :

- `afficher(self)` affiche la carte avec les positions et directions de tous les lemmings en jeu ;
- `tour(self)` fait « agir » chaque lemming une fois et affiche le nouvel état du jeu ;
dans cette méthode, il faut « regarder » ce qui se trouve autour de chaque lemming (mur, trou, …) afin de le faire agir en conséquence...
- `demarrer(self)` lance une boucle infinie attendant des commandes de l’utilisateur.
Exemples de commandes : « l » pour ajouter un lemming, « q » pour quitter le jeu,...
- ...

### Déroulement du jeu

La méthode `Jeu.demarrer` lance une boucle infinie, dont une issue passe par l’appui sur la touche "q"
(« quitter ») du clavier. Le rythme du jeu est donné par une constante de classe `Jeu.PERIODE`. Le calcul des positions des lemmings, suivi de l’affichage du jeu a lieu chaque `Jeu.PERIODE` secondes.

A chaque « tour », les lemmings font un mouvement (avancer, tomber ou se retourner), dans l’ordre de leur entrée dans le jeu. Lorsque l’un d’entre eux atteint la sortie, il quitte le jeu (on pourra par exemple à cette occasion incrémenter un attribut `score` du jeu)

Si l’utilisateur appuie sur la touche « l », un nouveau lemming rentre dans le jeu.

## Déroulement du projet

### Premier temps : prise en main sur Capytale

Compléter le fichier sur Capytale dont le code est `3355-841407`.

### Deuxième temps : créer l'interface graphique et animer le jeu

Téléchargez les fichiers [lemmings.py](lemmings.py) et [lemmings.txt](lemmings.txt).

A l'aide du logiciel Edupython (par exemple) complétez le fichier `lemmings.py` afin d'avoir une animation du jeu avec une interface utilisant Tkinter.


## Evolutions

Cette base peut ensuite évidemment être étendue avec des terrains plus variés, de nouvelles possibilités d’interaction pour le joueur, des objectifs en termes de nombre de lemmings sauvés, etc…


## Notation

- Programme : 10 points (un attention particulière sera mise sur la documentation et la clarté du code)
- Présentation orale : 10 points :
    - Présentation de 5 minutes (sans support autre qu'une feuille A4)
    - Questions pendant 10 minutes