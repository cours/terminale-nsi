class Lemming:
    def __init__(self, l, c, d):
        self.set_ligne(l)
        self.set_colonne(c)
        self.set_direction(d)

    def set_ligne(self, nouvelle_ligne):
        self.ligne = nouvelle_ligne

    def set_colonne(self, nouvelle_colonne):
        self.colonne = nouvelle_colonne

    def set_direction(self, nouvelle_direction):
        self.direction = nouvelle_direction

    def get_ligne(self):
        return self.ligne

    def get_colonne(self):
        return self.colonne

    def get_direction(self):
        return self.direction

    def __str__(self):
        if self.get_direction() == 1:
            affichage = '‍>'
        else:
            affichage = '<'
        return affichage

    def avancer(self):
        if self.get_direction() == 1:
            self.set_colonne(self.get_colonne() + 1)
        else:
            self.set_colonne(self.get_colonne() - 1)

    def retourner(self):
        if self.get_direction() == 1:
            self.set_direction(-1)
        else:
            self.set_direction(1)

    def tomber(self):
        self.set_ligne(self.get_ligne() - 1)


class Case:
    def __init__(self, terrain, lemming = None):
        self.set_terrain(terrain)
        self.set_lemming(lemming)

    def set_terrain(self, nouveau_terrain):
        self.terrain = nouveau_terrain

    def set_lemming(self, nouveau_lemming):
        self.lemming = nouveau_lemming

    def get_terrain(self):
        return self.terrain

    def get_lemming(self):
        return self.lemming

    def __str__(self):
        lemming = self.get_lemming()
        if lemming is None:
            affichage = self.get_terrain()
        else:
            if lemming.get_direction() == 1:
                affichage = '>'
            elif lemming.get_direction() == -1:
                affichage = '<‍'
            else:
                affichage = ' '
        return affichage

    def est_libre(self):
        if self.get_terrain() == ' ':
            return True
        else:
            return False

    def liberer(self, lem):
        self.set_lemming(None)
        self.set_terrain(' ')

    def occuper(self, lem):
        self.set_lemming(lem)
        if lem.get_direction() == 1:
                affichage = '>'
        elif lem.get_direction() == -1:
                affichage = '<‍'
        else:
                affichage = ' '
        self.set_terrain(affichage)


class Jeu:
    def __init__(self, grille_texte, lemmings):
        self.grille = []
        with open(grille_texte, 'r') as fichier:
            for ligne in fichier:
                Ligne = []
                for colonne in ligne:
                    c = Case(colonne)
                    Ligne.append(c)
                self.grille.append(Ligne)
        self.lemmings = lemmings
        self.score = 0

    def set_grille(self, l, c, changement_grille):
        self.grille[l][c] = changement_grille

    def set_lemmings(self, nouveau_lemmings):
        self.lemmings.append(nouveau_lemmings)

    def get_grille(self):
        return self.grille

    def get_lemmings(self):
        return self.lemmings

    def afficher(self):
        if self.get_lemmings() != []:
            for lem in self.get_lemmings():
                if lem.get_direction() != 0:
                    if lem.get_direction() == 1:
                        symbole = '‍>'
                    elif lem.get_direction() == -1:
                        symbole = '<'
                    self.set_grille(lem.get_ligne(), lem.get_colonne(), Case(symbole, lem))
            affichage = self.get_grille()
        else:
            affichage = self.get_grille()
        resultat = ''
        for ligne in affichage:
            for colonne in ligne:
                resultat = resultat + str(colonne)
        return resultat

    def tour(self):
        for lem in self.get_lemmings():
            if lem is not None:
                l = lem.get_ligne()
                c = lem.get_colonne()
                d = lem.get_direction()
                grille = self.get_grille()
                ici = grille[l][c]
                gauche = grille[l][c - 1]
                droite = grille[l][c + 1]
                bas = grille[l + 1][c]
                if d == 1:
                    if bas.est_libre():
                        ici.liberer(lem)
                        lem.set_direction(1)
                        l = l + 1
                        lem.set_ligne(l)
                        bas.occuper(lem)
                    elif droite.est_libre():
                        ici.liberer(lem)
                        lem.set_direction(1)
                        c = c + 1
                        lem.set_colonne(c)
                        droite.occuper(lem)
                    elif droite.get_terrain() == 'O':
                        ici.liberer(lem)
                        droite.set_terrain('O')
                        lem.set_direction(0)
                        self.score = self.score + 1
                    else:
                        lem.retourner()
                elif d == -1:
                    if bas.est_libre():
                        ici.liberer(lem)
                        lem.set_direction(-1)
                        l = l + 1
                        lem.set_ligne(l)
                        bas.occuper(lem)
                    elif gauche.est_libre():
                        ici.liberer(lem)
                        lem.set_direction(-1)
                        c = c - 1
                        lem.set_colonne(c)
                        gauche.occuper(lem)
                    elif gauche.get_terrain() == 'O':
                        ici.liberer(lem)
                        lem.set_direction(0)
                        gauche.set_terrain('O')
                        self.score = self.score + 1
                    else:
                        lem.retourner()

    def demarrer(self):
        while True:
            def animer():
                test.tour()
                c = test.afficher()
                titre.config(text = c)
                point.config(text = "Le score est de "+ str(self.score) + " lemming(s) sauvé(s)")
                fenetre.after(300, supprimer_texte)

            def supprimer_texte():
                titre.config(text = "")
                fenetre.after(0, animer)

            def attente():
                fenetre.after(300, supprimer_texte)

            def touche_clavier(event):
                t = event.keysym
                if t == "l":
                    self.lemmings.append(Lemming(1, 1, 1))
                if t == "q":
                    fenetre.destroy()
                

            # Animation
            fenetre = Tk()
            c = test.afficher()
            titre = Label(fenetre, text = c, justify='left', font=("Courier", 30))
            titre.pack()
            point = Label(fenetre, text = "Le score est de "+ str(self.score) + " lemming(s) sauvé(s)", font = ("Courier", 30))
            point.pack()
            attente()
            fenetre.bind("<Key>", touche_clavier)
            fenetre.mainloop()


from time import sleep
from tkinter import *

#lem1 = Lemming(1, 1, 1)
#lem2 = Lemming(3, 4, -1)
#lem3 = Lemming(3, 9, 1)
#lem4 = Lemming(1, 5, 1)
#lem5 = Lemming(5, 8, 1)
#lem6 = Lemming(1, 3, 1)
#test = Jeu('lemmings.txt', [lem1, lem2, lem3, lem4, lem5, lem6])
test = Jeu('lemmings.txt', [])
test.demarrer()


