# Algorithmes

## Diviser pour regner

### Introduction

L’expression «Diviser pour régner» est très ancienne : Les romains l’utilisaient au sénat («divide ut regnes»), Jules César l’ayant adapté en «divide et impera». Cette expression est reprise par le penseur Machiavel au XVIème siècle. La version anglaise donne «divide and conquer»!


Le but de ce type d'algorithme est d'optimiser la complexité en temps.

### Principe

On prend un problème (généralement complexe à résoudre), on divise ce problème en une multitude de petits problèmes, l'idée étant que les "petits problèmes" seront plus simples à résoudre que le problème original. Une fois les petits problèmes résolus, on recombine les "petits problèmes résolus" afin d'obtenir la solution du problème de départ.

Le paradigme "diviser pour régner" repose donc sur 3 étapes :

- **DIVISER** : le problème d'origine est divisé en un certain nombre de sous-problèmes.
- **REGNER** : on résout les sous-problèmes (les sous-problèmes sont plus faciles à résoudre que le problème d'origine)
- **COMBINER** : les solutions des sous-problèmes sont combinées afin d'obtenir la solution du problème d'origine.

Les algorithmes basés sur le paradigme "diviser pour régner" sont très souvent des algorithmes récursifs.

### Recherche dans un tableau

On dispose d'un tableau de valeurs, et on cherche à savoir si une valeur donnée est présente dans le tableau ou non.

#### Recherche naïve

On considère le programme suivant sous Python :

```python
def recherche(tableau, element):
    for valeur in tableau:
        if valeur == element:
            return True
    return False
```

Notons qu'il n'est pas possible de savoir si la valeur à rechercher est absente du tableau avant d'avoir examiné toutes les valeurs du tableau: à cause de cela, on en déduit immédiatemment que la complexité dans le pire des cas est proportionnelle à la taille du tableau $N$. On dit qu'il s'agit d'un algorithme de **complexité linéaire** : $O(n)$.

#### Recherche dans un tableau trié

Supposons à présent que les valeurs du tableau sont triées dans l'ordre croissant. Peut-on améliorer l'algorithme précédent afin de le rendre plus efficace ?

Une première amélioration — très simple — vient à l'esprit: dès que l'on tombe sur une valeur supérieur à la valeur à rechercher, on peut stopper la recherche car toutes les valeurs suivantes seront elles aussi supérieures. Puisque l'on souhaite stopper l'algorithme préventivement, il faut utiliser une boucle tant que.

```python
def recherche_ordonnee(tableau, element):
    i = 0
    N = len(tableau)
    while tableau[i] <= element and i < N:
        if tableau[i] == element:
            return True
        i = i + 1
    return False
```

Lorsque la boucle est terminée, on a soit la condition $i\ge N$ qui signifie que tout le tableau a été parcouru sans succès, soit la condition $tableau[i] > element$ qui signifie qu'il est inutile de poursuivre la recherche: on peut donc bien renvoyer False.

La complexité de cet algorithme est-elle améliorée ? Il est difficile d'évaluer si — et quand — la recherche pourra s'interrompre prématurément. En outre, il est toujours possible que la valeur à rechercher soit la dernière du tableau.

La complexité dans le pire des cas reste donc linéaire : $O(N)$

#### La recherche dichotomique

Il est possible de faire bien mieux que cela avec un tableau trié, en s'inspirant du jeu "plus petit - plus grand".

- On regarde l'élément au milieu du tableau: s'il est supérieur à la valeur recherchée, on sait que celle-ci ne pourra se trouver que dans la première moitié du tableau, et on réduit alors la recherche à cette première moitié. Si au contraire l'élément central est inférieur, on doit réduire la recherche à la deuxième moitié du tableau, car l'élément à rechercher ne peut se trouver que là.
- On peut poursuivre ainsi, en subdivisant à chaque étape les sous-tableaux en deux moitiés à peu près égales (à un élément près), et en recommençant à l'étape précédente.
- On finira par arriver à un tableau de longeur 1, contenant (ou pas) l'élément à rechercher.

Le fait de couper le tableau en deux parties donne son nom à l'algorithme : dichotomie signifie couper en deux.

Plus précisément, on va utiliser deux indices $g$ et $d$ (pour *gauche* et *droite*) en indiquant l'intervalle dans lequel doit s'effectuer la recherche.

Supposons que l'on dispose d'un tableau ordonnée *t* de longeur *N*.

1. Au départ, on aute $g=0$ et $d=N-1$.
2. On calcule l'indice au milieu de l'intervalle de recherche : $m=(g+d)//2$. On utilise une division euclidienne afin de s'assurer que cet indice est bien un entier.<br>
Trois cas peuvent se produire :
    - Si $t[m]==element$, on a trouvé la valeur à rechercher.
    - Sinon, si $t[m] < element$, la valeur à rechercherse trouve (peut-être) dans la seconde moitié. On gardera la valeur actuelle de *d*, mais on prendra à présent $g=m+1$. Reprendre à l'étape 1.
    - Sinon, on a nécessairement $t[m]>element$ et la valeur à rechercher est peut-être dans la première moitié. On gardera la valeur actuelle de *g* mais on posera $d=m-1$. Reprendre à l'étape 1.
3. Si à un moment donné, on a $g>d$ (ce qui est possible avec les isntructions $g=m+1$ ou bien $d=m-1$), on sait que l'on peut interrompre la recherche car la valeur ne se trouve pas dans le tableau.

On obtient ainsi le programme suivant de recherche dichotomique :

```python
def recherche_dichotomique(tableau, element):
    g = 0
    d = len(tableau) - 1
    while g <= d and tableau[g] <= element <= tableau[d]:
        m = (g + d) // 2
        if tableau[m] == element:
            return True
        elif tableau[m] < element:
            g = m + 1
        else:
            d = m - 1
    # La recherche n'a rien donné
    return False
```
La complexité est bien plus compliquée à évaluer. A chaque étape (passage de la boucle), le nombre de valeur à rechercher est à peu près divisé par 2.

Pourquoi *à peu près*? Plus précisément (et cela ressemble énormément au raisonnement utilisé en mathématique pour déterminer la médiane d'une série de données) :

- Si $L=d-g + 1$ est pair, alors à la moitié à gauche de la valeur d'indice $m = (g + d) // 2$ comportera $(L // 2) - 1$ valeurs, celle de droite en comportera $L // 2$. Le cas le plus défavorable étant $L // 2$, on considérera cette valeur pour nos calculs.
- Si $L = d - g + 1$ est impair, alors il y aura une valeur centrale d'indice $m = (g + d) // 2$, et les deux moitiés du tableau comporteront exactement $L // 2$ valeurs.

Dans tou les cas, *L* prendra pour valeur suivante $L//2$.

Voyons cela sur un exemple avec un tableau de taille $N=100$ :

- A la première étape, on gardera une moitié de taille 49 ou 50. Supposons que ce soit 50.
À l'étape suivante, on aura un tableau de taille 24 ou 25. Prenons le cas défavorable \(L = 25\).
À l'étape suivante, on aura un tableau nécessairement de taille 12.
À l'étape suivante, on aura un tableau de taille au plus 6.
- puis un tableau de taille au plus 3.
- puis un tableau de taille 1: la recherche s'arrêtera nécessairement à ce moment là (si le tableau contient l'élément à rechercher), ou bien à la suivante s'il n'y est pas.

Comptons : nous aurons au maximum 7 étapes (il peut y en avoir moins si l'élément à trouver est l'un des pivot d'indice *m* examiné par une boucle), c'est qui est largement inférieur à la valeur initiale $N = 100$.

Est-il possible de calculer à l'avance ce nombre 7 ? En fait oui: il suffit de remarquer que la plus petite puissance de 2 inférieure ou égale à 100 est $2^6 = 64$, la puissance de 2 immédiatemment supérieure étant $2^7 = 128$. C'est pour cela qu'il faut au maximum 7 étapes pour effectuer une recherche dichotomique.

Le principe est donc très simple: il suffit de trouver la plus petite puissance de 2 strictement supérieure à *N*, c'est-à-dire le plus petit entier *k* tel que $N < 2^k$, le nombre d'étapes dans le pire des cas sera alors *k*.

Il existe en mathématique une fonction réalisant exactement cela: c'est la fonction $\log_2$ (logarithme de base 2), qui renvoie exactement *N* lorsqu'elle est calculée sur $2^N$, et un nombre réel pour les valeurs intermédiaires. On a par exemple $\log_2(100) \approx 6,64$, ce qui est bien compris entre 6 et 7.

La complexité dans le pire des cas reste donc logarithmique : $O(\log_2(N))$.

#### La recherche dichotomique récursive

Rappelons l'explication informelle donnée initialement pour expliquer l'algorithme de recherche dichotomique en Première:

- On regarde l'élément au milieu du tableau: s'il est supérieur à la valeur recherchée, on sait que celle-ci ne pourra se trouver que dans la première moitié du tableau, et on réduit alors la recherche à cette première moitié. Si au contraire l'élément central est inférieur, on doit réduire la recherche à la deuxième moitié du tableau, car l'élément à rechercher ne peut se trouver que là.
- On peut poursuivre ainsi, en subdivisant à chaque étape les sous-tableaux en deux moitiés à peu près égales (à un élément près), et en recommençant à l'étape précédente.
- On finira par arriver à un tableau de longueur 1, contenant (ou pas) l'élément à rechercher.

Tel qu'il est écrit ici, on a l'impression qu'il est structuré autour d'une boucle (et c'est effectivement comme cela que l'on peut l'interpréter en Première), alors qu'en réalité il s'agit d'une définition récursive. Reformulons légèrement cet algorithme:

- Pour effectuer une recherche dichotomique sur un tableau, on commence par regarder l'élément au milieu du tableau: s'il est supérieur à la valeur recherchée, on sait que celle-ci ne pourra se trouver que dans la première moitié du tableau, et on effectue alors une recherche dichotomique sur cette première moitié. Si au contraire l'élément central est inférieur, on doit effectuer la recherche dichotomique sur à la deuxième moitié du tableau, car l'élément à rechercher ne peut se trouver que là.
- On s'arrête soit lorsque l'élément central est la valeur à rechercher, soit lorsque le sous-tableau ne contient plus aucune valeur (largeur inférieure à 0).

Cette fois, la récursivité intrinsèque de l'algorithme apparaît explicitement. Donnons-en une implémentation en python. Nous utiliserons les mêmes paramètres *g* et *d* que pour l'implémentation itérative de Première:

```python
def recherche_dichotomique_rec(tableau, element,  g, d):
    if g > d:
        # La "largeur" de la zone de recherche est négative: 
        # La recherche a été infructueuse
        return False
    else:
        # On sait que g <= d: la "largeur" de la zone est positive ou nulle
        # (nulle = il ne reste plus qu'un seul élément)
        m = (g + d) // 2
        if tableau[m] == element:
            return True
        elif tableau[m] < element :
            return recherche_dichotomique_rec(tableau, element, m + 1, d)
        else:
            return recherche_dichotomique_rec(tableau, element, g, m - 1)


def recherche_dichotomique(tableau, element):
    return recherche_dichotomique_rec(tableau, element, 0, len(tableau) - 1)
```

Cette implémentation récursive de l'algorithme est plus simple à comprendre que l'implémentation itérative, car elle est plus proche de la description initiale de l'algorithme.

La complexité de l'algorithme est exactement la même en version récursive qu'itérative, puisque le nombre d'appels récursifs est égal au nombre de passage de la boucle pour la version de Première.

Est-il possible de dépasser la pile d'appel de python (en général 1000 appels maximum) ? Difficilement, car pour cela il faudrait que la taille du tableau *N* soit supérieure à $2^{1000}$, ce qui donne environ $10^{300}$, un nombre largement supérieur au nombre estimé d'atomes de l'univers (l'ordre de grandeur est d'environ $10^{80}$). Aucune chance d'avoir assez de mémoire dans un ordinateur pour ne serait-ce que stocker un tableau aussi grand.

**Exercice 0**

Ouvrez le fichier Capytale dont le code est `5b37-1145568` pour visualiser les différents temps d'exécutions des différents programmes de recherches dans un tableau trié.

**Exercice 1**

Faire le TP sur Capytale avec le code `2a78-1145578`.

--8<-- "docs/10-Algorithmes/Corrections/Exercice1_correction.md"

### Le tri fusion

La méthodologie «diviser pour régner» peut être avantageusement utilisée pour trier une liste, donnant à la fois un algorithme simple à décrire, mais qui s'avérera aussi beaucoup plus efficace que les algorithmes de tri par insertion ou par sélection étudiés en classe de Première.

Le principe est schématisé ci-dessous :

![](Images/fusion.png){: .center}

- A chaque étape, on sépare la liste en deux sous-listes (flèches bleues sur le diagramme).
- Lorsque toutes les sous-listes ont été décomposées, on les fusionne deux par deux (flèches rouges). L'opération de fusion prend 2 listes déjà triées (ce qui est notamment le cas pour les listes de longueur 1), et les combine en une unique liste elle aussi triée.


La méthode décrite peut se résumer en l'algorithme naïf suivant :

```
fonction Tri_Fusion(Liste):
    Découper Liste en deux sous-listes L1 et L2
    Appliquer la fonction tri_fusion à L1
    Appliquer la fonction tri_fusion à L2
    Fusionner L1 et L2

    Renvoyer le résultat de la fusion
```

L'algorithme de tri fusion se prête bien à une implémentation à l'aide de listes.

Outre la fonction récursive *tri_fusion*, nous avons besoin d'une fonction *coupe* pour découper une liste en sous-listes, ainsi que d'une fonction *fusion*.

Sur le schémas précédents, la coupe a toujours été effectuée en milieu de liste. Mais cela n'est absolument pas une nécessité: l'essentiel est que l'on obtiennent deux sous-listes de tailles à peu près équivalentes (il peut y avoir une différence d'une unité), peu importe comment les éléments de la liste initiale ont été répartis :

```
Fonction  coupe(liste):
   """
   Découpe la liste  en deux sous-listes de tailles équivalentes.
   """

   l1 ← liste vide
   l2 ← liste vide

   Pour i allant de 0 à longueur de liste
   
      si  i est pair :
         ajouter l'élément de liste d'indice i à l1
      else:
         ajouter l'élément de liste d'indice i à l2

    Renvoyer l1, l2
```

La fonction de fusion peut se décrire facilement à l'aide d'un algorithme récursif :

```
Fonction fusion(l1,l2)
    """
    Fusionne les deux listes TRIÉES l1 et l2 en une unique liste triée.
    """
    
    N1 ← longueur de l1
    N2 ← longueur de l2
    
    liste ← liste vide
    i1 ← 0
    i2 ← 0
    Tant que  i1 < N1 ou i2 < N2: # tant qu'il reste des éléments à traiter dans au moins l'une des 2 listes
        Si i1 = N1 Alors
            # La première liste est déjà épuisée: on peut donc traiter exclusivement la deuxième
            Tant que i2 < N2 Faire
                Ajouter à liste l'élément d'incide i2 dans l2
                i2 ← i2 + 1
        Sinon Si i2 = N2 Alors
            # La deuxième liste est déjà épuisée: on peut donc traiter exclusivement la première
            Tant que i1 < N1 Faire
                Ajouter à liste l'élément d'incide i1 dans l1
                i1 ← i1+ 1
        Sinon
            # Il reste des éléments à traiter dans chacune des 2 listes: on garde le plus petit des 2
            Si l1[i1] < l2[i2] Alors
                Ajouter à liste l'élément d'incide i1 dans l1
                i1 ← i1 + 1
            Sinon
                Ajouter à liste l'élément d'incide i2 dans l2
                i2 ← i2 + 1
    
    Renvoyer liste
```

Il est tout fait possible (et intéressant) de remplacer cet algorithme récursif par un autre algorithme, itératif. L'avantage étant qu'avec l'algorithme récursif, il n'est pas aisé de fusionner des listes dont la taille dépasse la limite de récursion de python (normalement 1000 appels). Avec un algorithme itératif, aucun problème de ce genre.

Enfin, la fonction de tri proprement dite est une implémentation directe de l'algorithme de la section précédente:

```
Fonction tri_fusion(liste):
    """
    Renvoie une copie triée de `liste`.
    """
    
    # Une liste de longueur 0 ou 1 est déjà triée: il n'y a rien à faire
    Si liste est vide Alors
        Renvoyer []
    Sinon Si liste de longueur 1 Alors
        Renvoyer [liste[0]] # On doit renvoyer une *copie* de liste, pas la liste elle-même.
    Sinon:
        l1, l2 ← coupe(liste)
        lt1 ← tri_fusion(l1)
        lt2 ← tri_fusion(l2)
        
        Renvoyer fusion(lt1, lt2)
```

Quel est le nombre d'étapes nécessaires pour effectuer un tri fusion ?

La fusion elle-même prend exactement \(N\) étapes, où \(N\) est la taille de la liste fusionnée. Si on note \(c(N)\) le nombre d'étapes pour fusionner une liste de taille \(N\), alors l'algorithme récursif nous donne directement la relation

\[ c(N) = 2\times c(N/2) + N\]

En effet, on a compté le nombre d'étapes du tri fusion des deux sous-listes, ainsi que la fusion elle-même (on peut considérer que le découpage est, quand à lui, constant, même si ce n'est pas le cas dans l'implémentation proposée ici).

On peut montrer en mathématiques que la fonction *c(N)* est proportionnelle à $N\times\log_2(N)$ (cette démonstration est totalement hors-programme au lycée).

La complexité de l'algorithme de tri fusion est donc intermédiaire entre une complexité linéaire et une complexité quadratique. Cependant, nous verrons en TP que la complexité de l'algorithme de tri fusion est très largement supérieure à celle des tris par insertion ou sélection lorsque le nombre d'éléments devient grand.

Prenons par exemple le cas d'une liste de taille $N = 1000$. Un tri de complexité quadratique aurait un nombre d'étapes proportionnel à $1000^2 = 1\,000\,000$, alors que le tri fusion ne prendrait que $1000\times \log_2(1000) \approx 9966$ étapes. La différence est considérable, et l'écart ne ferait que se creuser avec une liste plus longue.

**Exercice 2**

Faire le TP sur Capytale dont le code est `882c-1145628`.

--8<-- "docs/10-Algorithmes/Corrections/Exercice2_correction.md"


## Programmation dynamique

### Suite de Fibonacci

Revenons sur ce qui a été vu dans le cours consacré à la récursivité. On vous a demandé d'écrire une fonction récursive qui permet de calculer le n-ième terme de la suite de Fibonacci. Voici normalement ce que vous avez dû obtenir : 

```python
def fib(n) :
    if n < 2 :
        return n
    else :
        return fib(n-1) + fib(n-2)
```

Pour $n=6$, il est possible d'illustrer le fonctionnement de ce programme avec le schéma ci-dessous :

![](Images/fibo1.png){: .center}

 Vous pouvez constater que l'on a une structure arborescente (typique dans les algorithmes récursifs), si on additionne toutes les feuilles de cette structure arborescente (`fib(1) = 1` et `fib(0) = 0`), on retrouve bien 8.

En observant attentivement le schéma ci-dessus, vous avez remarqué que de nombreux calculs sont inutiles, car effectué 2 fois : par exemple on retrouve le calcul de `fib(4)` à 2 endroits (en haut à droite et un peu plus bas à gauche) : 

![](Images/fibo2.png){: .center}

On pourrait donc grandement simplifier le calcul en calculant une fois pour toutes `fib(4)`, en "mémorisant" le résultat et en le réutilisant quand nécessaire :

**Exercice 3**

Après avoir étudié le programme ci-dessous, expliquez en quoi il permet de "mémoriser" et de réutiliser certains résultats, puis testez-le :

```python
def fib_mem(n):
    mem = [0]*(n+1)  #permet de créer un tableau contenant n+1 zéro
    return fib_mem_c(n, mem)

def fib_mem_c(n, m):
    if n == 0 or n == 1:
        m[n] = n
        return n
    elif m[n] > 0:
        return m[n]
    else:
        m[n] = fib_mem_c(n-1, m) + fib_mem_c(n-2, m)
        return m[n]
```

{{IDE('', ID=101)}}

 Dans le cas qui nous intéresse, on peut légitimement s'interroger sur le bénéfice de cette opération de "mémorisation", mais pour des valeurs de n beaucoup plus élevées, la question ne se pose même pas, le gain en termes de performance (temps de calcul) est évident. Pour des valeurs n très élevées, dans le cas du programme récursif "classique" (n'utilisant pas la "mémorisation"), on peut même se retrouver avec un programme qui "plante" à cause du trop grand nombre d'appels récursifs.

En réfléchissant un peu sur le cas que nous venons de traiter, nous divisons un problème "complexe" (calcul de fib(6)) en une multitude de petits problèmes faciles à résoudre (`fib(0)` et `fib(1)`), puis nous utilisons les résultats obtenus pour les "petits problèmes" pour résoudre le problème "complexe". Cela devrait vous rappeler la méthode "diviser pour régner" !

En faite, ce n'est pas tout à fait cela puisque dans le cas de la méthode "diviser pour régner", la "mémorisation" des calculs n'est pas prévue. La méthode que nous venons d'utiliser se nomme "programmation dynamique". 

### Programmation dynamique

 Comme nous venons de le voir, la programmation dynamique, comme la méthode diviser pour régner, résout des problèmes en combinant des solutions de sous-problèmes. Cette méthode a été introduite au début des années 1950 par Richard Bellman.

Il est important de bien comprendre que "programmation" dans "programmation dynamique", ne doit pas s'entendre comme "utilisation d'un langage de programmation", mais comme synonyme de planification et ordonnancement.

La programmation dynamique s'applique généralement aux problèmes d'optimisation. Nous avons déjà évoqué les problèmes d'optimisation lorsque nous avons étudié les **algorithmes gloutons** l'année dernière. N'hésitez pas, si nécessaire à vous replonger dans ce cours.

Comme déjà évoqué plus haut, à la différence de la méthode diviser pour régner, la programmation dynamique s'applique quand les sous-problèmes se recoupent, c'est-à-dire lorsque les sous-problèmes ont des problèmes communs (dans le cas du calcul de `fib(6)` on doit calculer 2 fois `fib(4)`. Pour calculer `fib(4)`, on doit calculer 4 fois `fib(2)`...). Un algorithme de programmation dynamique résout chaque sous-sous-problème une seule fois et mémorise sa réponse dans un tableau, évitant ainsi le recalcul de la solution chaque fois qu'il résout chaque sous-sous-problème.

### Rendu de monnaie

Nous allons maintenant travailler sur un problème d'optimisation déjà rencontré l'année dernière : le problème du rendu de monnaie.

Petit rappel : vous avez à votre disposition un nombre illimité de pièces de 2 cts, 5 cts, 10 cts, 50 cts et 1 euro (100 cts). Vous devez rendre une certaine somme (rendu de monnaie). Le problème est le suivant : "Quel est le nombre minimum de pièces qui doivent être utilisées pour rendre la monnaie"

La résolution "gloutonne" de ce problème peut être la suivante :

1. on prend la pièce qui a la plus grande valeur (il faut que la valeur de cette pièce soit inférieure ou égale à la somme restant à rendre)
2. on recommence l'opération ci-dessus jusqu'au moment où la somme à rendre est égale à zéro.

Prenons un exemple :

nous avons 1 euro 77 cts à rendre :

- on utilise une pièce de 1 euro (plus grande valeur de pièce inférieure à 1,77 euro), il reste 77 cts à rendre
- on utilise une pièce de 50 cts (plus grande valeur de pièce inférieure à 0,77 euro), il reste 27 cts à rendre
- on utilise une pièce de 10 cts (plus grande valeur de pièce inférieure à 0,27 euro), il reste 17 cts à rendre
- on utilise une pièce de 10 cts (plus grande valeur de pièce inférieure à 0,17 euro), il reste 7 cts à rendre
- on utilise une pièce de 5 cts (plus grande valeur de pièce inférieure à 0,07 euro), il reste 2 cts à rendre
- on utilise une pièce de 2 cts (plus grande valeur de pièce inférieure à 0,02 euro), il reste 0 cts à rendre

L'algorithme se termine en renvoyant 6 (on a dû rendre 6 pièces)

**Exercice 4**

Appliquez l'algorithme glouton vu ci-desus avec la somme à rendre égale à 11 centimes.

Comme vous l'avez sans doute remarqué, si on applique l'algorithme glouton à cet exemple, nous ne trouvons pas de réponse. En effet :

- on utilise une pièce de 10 cts (plus grande valeur de pièce inférieure à 11 centimes), il reste 1 cts à rendre
- il n'y a pas de pièce de 1 cts ⇒ l'algorithme est "bloqué"

Cet exemple marque une caractéristique importante des algorithmes glouton : une fois qu'une "décision" a été prise, on ne revient pas "en arrière" (on a choisi la pièce de 10 cts, même si cela nous conduit dans une "impasse").

Rappel : dans certains cas, un algorithme glouton trouvera une solution, mais cette dernière ne sera pas "une des meilleures solutions possible" (une solution optimale), voir le cours sur les algorithmes glouton.

Évidemment, le fait que notre algorithme glouton ne soit pas "capable" de trouver une solution ne signifie pas qu'il n'existe pas de solution...en effet, il suffit de prendre 1 pièce de 5 cts et 3 pièces de 2 cts pour arriver à 11 cts. Recherchons un algorithme qui nous permettrait de trouver une solution optimale, quelle que soit la situation.

Afin de mettre au point un algorithme, essayons de trouver une relation de récurrence :

Soit $X$ la somme à rendre, on notera $Nb(X)$ le nombre minimum de pièces à rendre. Nous allons nous poser la question suivante : Si je suis capable de rendre $X$ avec $Nb(X)$ pièces, quelle somme suis-je capable de rendre avec $1+Nb(X)$ pièces ?

Si j'ai à ma disposition la liste de pièces suivante : $p_1, p_2, p_3, ..., p_n$ et que je suis capable de rendre $X$ cts, je suis donc aussi capable de rendre : 

- $X−p_1$
- $X−p_2$
- $X−p_3$
- ...
- $X−p_n$

(à condition que $p_i$ (avec *i* compris entre 1 et n) soit inférieure ou égale à la somme restant à rendre) 

*Exemple* : si je suis capable de rendre 72 cts et que j'ai à ma disposition des pièces de 2 cts, 5 cts, 10 cts, 50 cts et 1 euro, je peux aussi rendre :

- 72 - 2 = 70 cts
- 72 - 5 = 67 cts
- 72 - 10 = 62 cts
- 72 - 50 = 22 cts

Je ne peux pas utiliser de pièce de 1 euro.

Autrement dit, si $Nb(X−p_i)$
(avec *i* compris entre 1 et n) est le nombre minimal de pièces à rendre pour le montant $X−p_i$, alors $Nb(X)=1+Nb(X−p_i)$ est le nombre minimal de pièces à rendre pour un montant *X*. Nous avons donc la formule de récurrence suivante :

- si $X=0$ : $Nb(X)=0$
- si $X>0$ : $Nb(X)=1+min(Nb(X−p_i))$ avec $1\le i<n$ et $p_i\le X$

Le "min" présent dans la formule de récurrence exprime le fait que le nombre de pièces à rendre pour une somme $X−p_i$
doit être le plus petit possible. 

**Exercice 5**

Étudiez attentivement le programme Python suivant :

```python
def rendu_monnaie_rec(P, X):
    if X==0:
        return 0
    else:
        mini = 1000
    for i in range(len(P)):
        if P[i] <= X:
            nb = 1 + rendu_monnaie_rec(P, X-P[i])
            if nb < mini:
                mini = nb
    return mini
pieces = (2,5,10,50,100)
```

Comme vous l'avez sans doute remarqué, pour être sûr de renvoyer le plus petit nombre de pièces, on attribue dans un premier temps la valeur 1000 à la variable mini (cette valeur 1000 est arbitraire, il faut juste une valeur suffisamment grande : on peut partir du principe que nous ne rencontrerons jamais de cas où il faudra rendre plus de 1000 pièces), ensuite, à chaque appel récursif, on "sauvegarde" le plus petit nombre de pièces dans cette variable mini. 

**Exercice 6**

Testez le programme de l'exercice 5 en saisissant dans la console Python `rendu_monnaie_rec(pieces,11)` (on recherche le nombre minimum de pièces à rendre pour une somme de 11 cts. Les pièces disponibles sont : 2 cts, 5 cts, 10 cts, 50 cts, 100 cts (1 euro))

{{IDE('', ID=102)}}

Le programme de l'exercice 5 n'est pas des plus simple à comprendre (même si on retrouve bien la formule de récurrence définit un peu au-dessus), voici un schéma (avec une somme à rendre de 11 centimes) qui vous permettra de mieux comprendre le principe de cet algorithme : 

![](Images/arbre.png){: .center}

 Plusieurs remarques s'imposent :

- comme vous pouvez le remarquer sur le schéma, tous les cas sont "traités" (quand un algorithme "traite" tous les cas possibles, on parle souvent de méthode "brute force").
- pour certains cas, on se retrouve dans une "impasse" (cas où on termine par un "1"), dans cette situation, la fonction renvoie "1000" ce qui permet de s'assurer que cette "solution" (qui n'en est pas une) ne sera pas "retenue".
- la profondeur minimum de l'arbre (avec une feuille 0) est de 4, la solution au problème est donc 4 (il existe plusieurs parcours : (5,2,2,2), (2,5,2,2)... qui donne à chaque fois 4)

**Exercice 7**

Testez le programme de l'exercice 5 en saisissant dans la console Python `rendu_monnaie_rec(pieces,171)` (on recherche le nombre minimum de pièces à rendre pour une somme de 1,71 euro. Les pièces disponibles sont : 2 cts, 5 cts, 10 cts, 50 cts, 100 cts (1 euro)). 

{{IDE('', ID=103)}}

 Comme vous pouvez le constater le programme ne permet pas d'obtenir une solution, pourquoi ? Parce que les appels récursifs sont trop nombreux, on dépasse la capacité de la pile.

Comme vous avez peut-être déjà dû le remarquer, même dans le cas simple évoqué ci-dessus (11 cts à rendre), nous faisons plusieurs fois exactement le même calcul. Par exemple on retrouve 2 fois la branche qui part de "4" : 

![](Images/arbre2.png){: .center}

 Il va donc être possible d'appliquer la même méthode que pour Fibonacci : la programmation dynamique.

À noter que dans des cas plus "difficiles à traiter" comme 1,71 euro, on va retrouver de nombreuses fois exactement les mêmes calculs, il est donc potentiellement intéressant d'utiliser la programmation dynamique. 

**Exercice 8**

Étudiez attentivement le programme Python suivant : 

```python
def rendu_monnaie_mem(P, X):
  mem = [0]*(X+1)
  return rendu_monnaie_mem_c(P, X, mem)


def rendu_monnaie_mem_c(P, X, m):
    if X == 0:
        return 0
    elif m[X] > 0:
        return m[X]
    else:
        mini = 1000
        for i in range(len(P)):
            if P[i] <= X:
                nb = 1 + rendu_monnaie_mem_c(P, X-P[i], m)
                if nb < mini:
                    mini = nb
                    m[X] = mini
    return mini
pieces = (2,5,10,50,100)
```

Ce programme ressemble beaucoup à programme utiliser pour la suite de Fibonacci, il ne devrait donc pas vous poser de problème. 

**Exercice 9**

 Testez le programme de l'exercice 8 en saisissant dans la console Python `rendu_monnaie_mem(pieces,171)` (on recherche le nombre minimum de pièces à rendre pour une somme de 1,71 euro. Les pièces disponibles sont : 2 cts, 5 cts, 10 cts, 50 cts, 100 cts (1 euro)).

{{IDE('', ID=104)}}

Comme vous pouvez le constater, au contraire du cas précédent (programme de l'exercice 5), il suffit d'une fraction de seconde pour que le programme basé sur la programmation dynamique donne une réponse correcte. 

**Exercice 10**

Faire une comparaison en temps d'exécution pour les deux programmes de Fibonacci, puis pour les deux programmes de rendue de monnaie.

{{IDE('', ID=105)}}